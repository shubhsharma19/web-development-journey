#  JavaScript Notes (JS) 


**Tip: use `Ctrl + click` to open links in a new tab.**

| Serial | Topics | Link |
|:--:|:-----------|:--|
|01|Intro and History of JavaScript Programming Language|[open](./intro/intro_history.md)|
|02|V8 Engine|[open](./v8_engine/v8.md)|
|03|JavaScript with browsers|[open](./javascript_with_browser/js_with_browser.md)|
|04|Variables|[open](./variables/variables.md)|
|05|Naming conventions|[open](./naming_conventions/naming_conventions.md)|
|06|Datatypes|[open](./datatypes/datatypes.md)|
|07|Comments|[open](./comments/comments.md)|
|08|Operators|[open](./operators/operators.md)|
|09|If and else conditions|[open](./control_structures/if_else.md)|
|10|Switches|[open](./control_structures/switches.md)|
|11|Loops|[open](./control_structures/loops.md)|
|12|Strings|[open](./strings/strings.md)|
|13|Arrays [ ]|[open](./arrays/arrays.md)|
|14|Objects { : }|[open](./objects/objects.mds)|
|15|Methods and Properties|[open](./functions/methods.md)|
|16|Stack vs. Heap memory|[open](./memory/stacksvsheapmemory.md)|
|17|Shallow vs. Deep Copy|[open](./shallow_vs_deep/shallow_vs_deep_copy.md)|
|18|Date and Time|[open](./date_time/date_time.md)|
|19|Vanilla Functions|[open](./functions/vanilla_functions.md)|
|20|Local vs. Global Scope|[open](./scopes/local_and_global_scope.md)|
|21|Closure|[open](./scopes/closure.md)|
|22|Function Expression|[open](./functions/function_expression.md)|
